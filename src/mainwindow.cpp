#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::createNewTab(const QString& title, CSVModel* model)
{
	// Create display table
	QTableView* table = new QTableView(this);
	table->setModel(model);
    table->setSelectionMode(QAbstractItemView::SelectionMode::MultiSelection);
    table->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectItems);
    table->setEditTriggers(QAbstractItemView::EditTrigger::DoubleClicked);

	// Add to UI
    ui->tabWidget->insertTab(ui->tabWidget->count(), table, QIcon(":/16x16/table.png"), title);
	ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);
    connect(model, &CSVModel::unsavedChanges, this, &MainWindow::onUnsavedChanges);
    connect(model, &CSVModel::savedFile, this, &MainWindow::onFileSaved);
}

/* Utility functions *****************************************************************************/

void MainWindow::closeTab(int index)
{
	if (ui->tabWidget->count() == 0) {
		return;
	}

	if (index == -1) {
		index = ui->tabWidget->currentIndex();
	}

	QTableView* table = getTabTable(index);
	CSVModel* model = getTabModel(index);

	if (model->hasUnsavedChanges()) {
		QMessageBox msgBox;
		msgBox.setText(tr("Tab has unsaved changes!"));
		msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
		msgBox.setDefaultButton(QMessageBox::Save);

		QString filePath = model->filePath();

		if (filePath.isEmpty()) {
			msgBox.setInformativeText(tr("Would you like to save the changes before closing it?"));
		} else {
			msgBox.setInformativeText(tr("Would you like to save the changes to ") + filePath + tr(" before closing it?"));
		}

		switch (msgBox.exec()) {
			case QMessageBox::Save:
				model->saveChanges();
				break;
			case QMessageBox::Cancel:
				return;
		}
	}

	ui->tabWidget->removeTab(index);
	delete table;
	delete model;
}

QTableView *MainWindow::getTabTable(int index)
{
    if (ui->tabWidget->count() == 0) {
        return nullptr;
    }

	if (index == -1) {
		index = ui->tabWidget->currentIndex();
	}

	return dynamic_cast<QTableView*>(ui->tabWidget->widget(index));
}

CSVModel* MainWindow::getTabModel(int index)
{
    if (ui->tabWidget->count() == 0) {
        return nullptr;
    }

	if (index == -1) {
		index = ui->tabWidget->currentIndex();
	}

	QTableView* table = getTabTable(index);
	return dynamic_cast<CSVModel*>(table->model());
}

void MainWindow::exitApplication()
{
	for (int i = 0; i < ui->tabWidget->count(); i++) {
		closeTab(i);
	}

	QApplication::quit();
}

void MainWindow::insertRow(bool after)
{
    auto table = getTabTable();
    auto model = getTabModel();

    if (table == nullptr || model == nullptr) {
        return;
    }

    if (model->rowCount() == 0) {
        // No other rows exist
        model->insertRow(0);
        return;
    }

    auto selection = table->selectionModel();
    auto rows = selection->selectedRows();

    if (rows.length() != 1) {
        QMessageBox alert;

        if (rows.length() == 0) {
            alert.setText(tr("No row is selected."));
        } else if (rows.length() > 1) {
            alert.setText(tr("Multiple rows are selected."));
        }

        alert.setIcon(QMessageBox::Icon::Warning);
        alert.exec();
        return;
    }

    model->insertRow(rows.first().row() + after);
}

void MainWindow::insertColumn(bool after)
{
    auto table = getTabTable();
    auto model = getTabModel();

    if (table == nullptr || model == nullptr) {
        return;
    }

    if (model->columnCount() == 0) {
        // No other columns exist
        model->insertColumn(0);
        return;
    }

    auto selection = table->selectionModel();
    auto columns = selection->selectedColumns();

    if (columns.length() != 1) {
        QMessageBox alert;

        if (columns.length() == 0) {
            alert.setText(tr("No column is selected."));
        } else if (columns.length() > 1) {
            alert.setText(tr("Multiple columns are selected."));
        }

        alert.setIcon(QMessageBox::Icon::Warning);
        alert.exec();
        return;
    }

    model->insertColumn(columns.first().column() + after);
}

/* Action handlers *******************************************************************************/

void MainWindow::onUnsavedChanges(CSVModel* model)
{
    // Get tab index for this model
    int i = 0;
    for (; i < ui->tabWidget->count(); i++) {
        if (getTabModel(i) == model) {
            break;
        }
    }

    if (model->hasUnsavedChanges()) {
        ui->tabWidget->setTabIcon(i, QIcon(":/16x16/table_refresh.png"));
    } else {
        ui->tabWidget->setTabIcon(i, QIcon(":/16x16/table.png"));
    }
}

void MainWindow::onFileSaved(QString file)
{
    statusBar()->showMessage(tr("Saved file ") + file);
}

void MainWindow::on_tabWidget_tabCloseRequested(int index)
{
	closeTab(index);
}

void MainWindow::on_actionSave_triggered()
{
    auto model = getTabModel();

    if (model != nullptr) {
        model->saveChanges();
    }
}

void MainWindow::on_actionOpen_triggered()
{
	QFileDialog dialog(this);
	dialog.setViewMode(QFileDialog::Detail);
	dialog.setFileMode(QFileDialog::FileMode::ExistingFiles);
	dialog.setNameFilter(tr("CSV files (*.csv)"));
	QStringList fileNames;

	if (dialog.exec()) {
		fileNames = dialog.selectedFiles();
	}

	if (fileNames.length() > 0) {
		for (const QString& fileName : fileNames) {
			QFileInfo file(fileName);
			createNewTab(file.fileName(), new CSVModel(this, fileName));
		}
	}
}

void MainWindow::on_actionNew_triggered()
{
	createNewTab(tr("New document"), new CSVModel(this));
}

void MainWindow::on_actionCloseTab_triggered()
{
	closeTab();
}

void MainWindow::on_actionExit_triggered()
{
	exitApplication();
}

void MainWindow::on_actionSaveAs_triggered()
{
    // FIXME: implement Save As
}

void MainWindow::on_actionSaveAll_triggered()
{
    for (int i = 0; i < ui->tabWidget->count(); i++) {
        getTabModel(i)->saveChanges();
    }
}

void MainWindow::on_actionCopy_triggered()
{
    // FIXME: implement Copy
}

void MainWindow::on_actionCut_triggered()
{
    // FIXME: implement Cut
}

void MainWindow::on_actionPaste_triggered()
{
    // FIXME: implement Paste
}

void MainWindow::on_actionSelectAll_triggered()
{
    auto table = getTabTable();

    if (table != nullptr) {
        table->selectAll();
    }
}

void MainWindow::on_actionSelectNone_triggered()
{
    auto table = getTabTable();

    if (table != nullptr) {
        table->clearSelection();
    }
}

void MainWindow::on_actionUndo_triggered()
{
    // FIXME: implement Undo
}

void MainWindow::on_actionRedo_triggered()
{
    // FIXME: implement Redo
}

void MainWindow::on_actionFind_triggered()
{
    // FIXME: implement Find
}

void MainWindow::on_actionReplace_triggered()
{
    // FIXME: implement Replace
}

void MainWindow::on_actionSort_triggered()
{
    // FIXME: implement Sort
}

void MainWindow::on_actionToggleHeaderRow_triggered()
{
    auto model = getTabModel();

    if (model != nullptr) {
        model->toggleHeaderRow();
        statusBar()->showMessage(tr("Header row ") + (model->hasHeaderRow() ? tr("enabled") : tr("disabled")));
    }
}

void MainWindow::on_actionDeleteRows_triggered()
{
    auto table = getTabTable();
    auto model = getTabModel();

    if (table == nullptr || model == nullptr) {
        return;
    }

    int deleted = 0;
    QItemSelectionModel* selection;
    while ((selection = table->selectionModel())->selectedRows().count() > 0) {
        model->removeRow(selection->selectedRows().first().row());
        deleted++;
    }

    statusBar()->showMessage(QString::number(deleted) + tr(" row") + (deleted == 1 ? "" : "s") + tr(" deleted"));
}

void MainWindow::on_actionDeleteColumns_triggered()
{
    auto table = getTabTable();
    auto model = getTabModel();

    if (table == nullptr || model == nullptr) {
        return;
    }

    int deleted = 0;
    QItemSelectionModel* selection;
    while ((selection = table->selectionModel())->selectedColumns().count() > 0) {
        model->removeColumn(selection->selectedColumns().first().column());
        deleted++;
    }

    statusBar()->showMessage(QString::number(deleted) + tr(" column") + (deleted == 1 ? "" : "s") + tr(" deleted"));
}

void MainWindow::on_actionInsertRowAbove_triggered()
{
    insertRow(false);
}

void MainWindow::on_actionInsertRowBelow_triggered()
{
    insertRow(true);
}

void MainWindow::on_actionInsertColumnLeft_triggered()
{
    insertColumn(false);
}

void MainWindow::on_actionInsertColumnRight_triggered()
{
    insertColumn(true);
}
