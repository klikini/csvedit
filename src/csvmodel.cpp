#include "csvmodel.h"

CSVModel::CSVModel(QObject *parent)
	: QAbstractTableModel(parent)
{
}

CSVModel::CSVModel(QObject *parent, QString fileName) : QAbstractTableModel(parent), file(fileName)
{
    if (!file.open(QFile::OpenModeFlag::Text | QFile::OpenModeFlag::ReadOnly)) {
		// FIXME: show error in parent
		return;
	}

	QTextStream stream(&file);

    int lines = 0;
	while (!stream.atEnd()) {
        std::vector<QString> row;

		std::string line = stream.readLine().toStdString();
		boost::tokenizer<boost::escaped_list_separator<char>> tok(line);

		for (auto field = tok.begin(); field != tok.end(); ++field) {
            row.push_back(QString::fromStdString(*field));
		}

        table.push_back(row);
        lines++;
	}

    file.close();
}

CSVModel::~CSVModel()
{
	if (file.isOpen()) {
		file.close();
	}
}

QVariant CSVModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Orientation::Vertical && (role == Qt::DisplayRole || role == Qt::EditRole)) {
        return QString::number(section + 1);
    }

    if (_hasHeaderRow && orientation == Qt::Orientation::Horizontal && (role == Qt::DisplayRole || role == Qt::EditRole)) {
        auto column = static_cast<unsigned long>(section);
        return table[0][column];
    }

    return QVariant::Invalid;
}

bool CSVModel::setHeaderData(int, Qt::Orientation, const QVariant&, int)
{
	return false;
}


int CSVModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    int rows = static_cast<int>(table.size());

    if (rows > 0 && _hasHeaderRow) {
        rows--;
    }

    return rows;
}

int CSVModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    if (table.size() == 0) {
        return 0;
    }

    return static_cast<int>(table[0].size());
}

QVariant CSVModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        auto row = static_cast<unsigned long>(index.row()) + _hasHeaderRow;
        auto column = static_cast<unsigned long>(index.column());
        QString value = table[row][column];
        return value;
    }

    return QVariant::Invalid;
}

bool CSVModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if ((role == Qt::DisplayRole || role == Qt::EditRole) && data(index, role) != value) {
        auto row = static_cast<unsigned long>(index.row()) + _hasHeaderRow;
        auto column = static_cast<unsigned long>(index.column());
        table[row][column] = value.toString();
		emit dataChanged(index, index, QVector<int>() << role);
        hasUnsavedChanges(true);
		return true;
	}

	return false;
}

Qt::ItemFlags CSVModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) {
		return Qt::NoItemFlags;
    }

    return Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

bool CSVModel::insertRows(int row, int count, const QModelIndex &parent)
{
    if (count == 0) {
        return false;
    }

    row += _hasHeaderRow;
	beginInsertRows(parent, row, row + count - 1);

    for (int i = 0; i < count; i++) {
        table.insert(table.begin() + row, {});
    }

	endInsertRows();
    hasUnsavedChanges(true);
    return true;
}

bool CSVModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    if (count == 0) {
        return false;
    }

	beginInsertColumns(parent, column, column + count - 1);

    for (int i = 0; i < rowCount(); i++) {
        std::vector<QString>* row = &(table[static_cast<unsigned long>(i)]);
        row->insert(row->begin() + column, QString());
    }

	endInsertColumns();
    hasUnsavedChanges(true);
    return true;
}

bool CSVModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (count == 0) {
        return false;
    }

    row += _hasHeaderRow;
    beginRemoveRows(parent, row, row + count - 1);
    table.erase(table.begin() + row, table.begin() + row + count);

    endRemoveRows();
    hasUnsavedChanges(true);
    return true;
}

bool CSVModel::removeColumns(int column, int count, const QModelIndex &parent)
{   
    if (count == 0) {
        return false;
    }

    beginRemoveColumns(parent, column, column + count - 1);

    for (int i = 0; i < rowCount() + _hasHeaderRow; i++) {
        std::vector<QString>* row = &(table[static_cast<unsigned long>(i)]);
        row->erase(row->begin() + column, row->begin() + column + count);
    }

    endRemoveColumns();
    hasUnsavedChanges(true);
    return true;
}

void CSVModel::toggleHeaderRow()
{
    _hasHeaderRow = !_hasHeaderRow;
    emit headerDataChanged(Qt::Orientation::Horizontal, 0, columnCount());
}

bool CSVModel::hasHeaderRow()
{
    return _hasHeaderRow;
}

QString CSVModel::filePath()
{
	return QFileInfo(file).absoluteFilePath();
}

bool CSVModel::hasUnsavedChanges()
{
    return _hasUnsavedChanges;
}

void CSVModel::hasUnsavedChanges(bool state)
{
    _hasUnsavedChanges = state;
    emit unsavedChanges(this);
}

bool CSVModel::saveChanges()
{
    if (!file.open(QFile::OpenModeFlag::Text | QFile::OpenModeFlag::WriteOnly)) {
        // FIXME: show error in parent
        return false;
    }

    for (auto row : table) {
        for (auto column = row.begin(); column != row.end(); column++) {
            file.write("\"");
            file.write(column->toUtf8());
            file.write("\"");

            if (std::next(column) != row.end()) {
                file.write(",");
            }
        }

        // End of line
        file.write("\n");
    }

    file.close();
    hasUnsavedChanges(false);
    emit savedFile(filePath());
    return true;
}
