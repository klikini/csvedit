#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFile>
#include <QFileDialog>
#include <QMainWindow>
#include <QMessageBox>
#include <QStatusBar>

#include "csvmodel.h"

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget* parent = nullptr);
	~MainWindow();
private slots:
    void onUnsavedChanges(CSVModel*);
    void onFileSaved(QString);
	void on_actionSave_triggered();
	void on_actionOpen_triggered();
	void on_tabWidget_tabCloseRequested(int index);
	void on_actionNew_triggered();
	void on_actionCloseTab_triggered();
	void on_actionExit_triggered();
    void on_actionSaveAs_triggered();
    void on_actionSaveAll_triggered();
    void on_actionCopy_triggered();
    void on_actionCut_triggered();
    void on_actionPaste_triggered();
    void on_actionSelectAll_triggered();
    void on_actionUndo_triggered();
    void on_actionRedo_triggered();
    void on_actionFind_triggered();
    void on_actionReplace_triggered();
    void on_actionSort_triggered();
    void on_actionToggleHeaderRow_triggered();
    void on_actionDeleteRows_triggered();
    void on_actionDeleteColumns_triggered();
    void on_actionSelectNone_triggered();
    void on_actionInsertRowAbove_triggered();
    void on_actionInsertRowBelow_triggered();
    void on_actionInsertColumnLeft_triggered();
    void on_actionInsertColumnRight_triggered();

private:
	Ui::MainWindow *ui;
	void createNewTab(const QString& title, CSVModel* model);
	void closeTab(int index = -1);
    void insertRow(bool after);
    void insertColumn(bool after);
	QTableView* getTabTable(int index = -1);
	CSVModel* getTabModel(int index = -1);
	void exitApplication();

};
#endif // MAINWINDOW_H
