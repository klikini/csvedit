#ifndef CSVMODEL_H
#define CSVMODEL_H

#include <QAbstractTableModel>
#include <QFile>
#include <QFileInfo>
#include <QTableView>
#include <QTextStream>

#include <vector>

#include <boost/tokenizer.hpp>

class CSVModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	explicit CSVModel(QObject *parent);
	explicit CSVModel(QObject *parent, QString fileName);
	~CSVModel() override;

	// Header:
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;
    void toggleHeaderRow();
    bool hasHeaderRow();

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	// Editable:
	bool setData(const QModelIndex &index, const QVariant &value,
				 int role = Qt::EditRole) override;

	Qt::ItemFlags flags(const QModelIndex& index) const override;

	// Add data:
	bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
	bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

	// Remove data:
	bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
	bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

    // Data backing:
    std::vector<std::vector<QString>> table;
	QString filePath();
	bool hasUnsavedChanges();
    void hasUnsavedChanges(bool state);
	bool saveChanges();

private:
	QFile file;
    bool _hasUnsavedChanges = false;
    bool _hasHeaderRow = false;

signals:
    void unsavedChanges(CSVModel*);
    void savedFile(QString file);
};

#endif // CSVMODEL_H
